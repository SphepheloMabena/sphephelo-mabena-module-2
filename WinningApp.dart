class WinningApp {
  var name = "Easy Equities";
  var sector = "Investment/Finance";
  var developer = "Purple Group";
  var year = 2020;

  void transformAppName() {
    var transformed = name.toUpperCase();
    print("Transformed Name : ${transformed}");
  }
}

void main() {
  var appInfo = WinningApp();
  print("Name : ${appInfo.name}");
  print("Sector : ${appInfo.sector}");
  print("Developer : ${appInfo.developer}");
  print("Year : ${appInfo.year}");

  appInfo.transformAppName();
}
